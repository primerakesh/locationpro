from rest_framework.serializers import ModelSerializer
from website.models import Region, Country, State, City, Area


class RegionSerializer(ModelSerializer):
    def __init__(self, *args, **kwargs):
        super(RegionSerializer, self).__init__(*args, **kwargs)

    class Meta:
        model = Region
        fields = '__all__'
        depth = 1


class CountrySerializer(ModelSerializer):
    def __init__(self, *args, **kwargs):
        super(CountrySerializer, self).__init__(*args, **kwargs)

    class Meta:
        model = Country
        fields = '__all__'
        depth = 1

class StateSerializer(ModelSerializer):
    def __init__(self, *args, **kwargs):
        super(StateSerializer, self).__init__(*args, **kwargs)

    class Meta:
        model = State
        fields = '__all__'
        depth = 1

class CitySerializer(ModelSerializer):
    def __init__(self, *args, **kwargs):
        super(CitySerializer, self).__init__(*args, **kwargs)

    class Meta:
        model = City
        fields = '__all__'

class AreaSerializer(ModelSerializer):
    def __init__(self, *args, **kwargs):
        super(AreaSerializer, self).__init__(*args, **kwargs)

    class Meta:
        model = Area
        fields = '__all__'
