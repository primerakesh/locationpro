from django.contrib import admin
from website.models import Region, Country, State, City, Area

admin.site.register(Region)
admin.site.register(Country)
admin.site.register(State)
admin.site.register(City)
admin.site.register(Area)
