from django.urls import path
from website.views import (
    RegionListAPIView,
    CountryListAPIView,
    StateListAPIView,
    CityListAPIView,
    AreaListAPIView,
)


urlpatterns = [
    path('region/',
         RegionListAPIView.as_view(),
         name='RegionListAPIView'),
    path('region/<str:region>/',
         CountryListAPIView.as_view(),
         name='CountryListAPIView'),
    path('region/<str:region>/<str:country>/',
         StateListAPIView.as_view(),
         name='StateListAPIView'),
    path('region/<str:region>/<str:country>/<str:state>/',
         CityListAPIView.as_view(),
         name='CityListAPIView'),
    path('region/<str:region>/<str:country>/<str:state>/<str:city>/',
         AreaListAPIView.as_view(),
         name='AreaListAPIView')
]
